#ifndef WORLDPHYS_H
#define WORLDPHYS_H

#include <iostream>
#include <vector>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include<glm/gtc/matrix_transform.hpp>

#include<glm/gtc/type_ptr.hpp>
#include<glm/gtc/matrix_inverse.hpp>

#include "object.h"

#include <map>

#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

#include <btBulletCollisionCommon.h>
#include <btBulletDynamicsCommon.h>
struct bulletObject {
	int id;
	float r, g, b;
	bool hit;
	btRigidBody* body;
	bulletObject(btRigidBody* bod0, int i, float r0, float g0, float b0) : body(bod0), id(i), r(r0), g(g0), b(b0), hit(false) {}
};

class WorldPhys
{
public:

	btDiscreteDynamicsWorld* dynamicsWorld;
	std::vector<bulletObject*> bodies;
	btBroadphaseInterface* broadphase;
	btDefaultCollisionConfiguration* collisionConfiguration;
	btCollisionDispatcher* dispatcher;
	btSequentialImpulseConstraintSolver* solver;

	WorldPhys() {
		// Build the broadphase
		broadphase = new btDbvtBroadphase();

		// Set up the collision configuration and dispatcher
		collisionConfiguration = new btDefaultCollisionConfiguration();
		dispatcher = new btCollisionDispatcher(collisionConfiguration);

		// The actual physics solver
		solver = new btSequentialImpulseConstraintSolver;

		// The world.
		dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, broadphase, solver, collisionConfiguration);
		dynamicsWorld->setGravity(btVector3(0, -9.81f, 0));
	}


	void addSphere(float r, float x, float y, float z, float mass, btVector3 speed = btVector3(0.0, 0.0, 0.0)) {
		btTransform t;
		t.setIdentity();
		t.setOrigin(btVector3(x, y, z));
		btSphereShape* sphereShape = new btSphereShape(btScalar(r));
		btMotionState* motionSphere = new btDefaultMotionState(t);
		btRigidBody::btRigidBodyConstructionInfo infoSphere(
			mass,						//Mass
			motionSphere,						//MotionState
			sphereShape,					//MotionState
			btVector3(0.0, 0.0, 0.0)		//Inertia
		);
		sphereShape->calculateLocalInertia(infoSphere.m_mass, infoSphere.m_localInertia);
		btRigidBody* sphereBody = new btRigidBody(infoSphere);
		sphereBody->setLinearVelocity(speed);
		dynamicsWorld->addRigidBody(sphereBody);
		sphereBody->setUserIndex(bodies.size());
		bodies.push_back(new bulletObject(sphereBody, bodies.size(), 0.0, 0.0, 0.0));
		sphereBody->setCollisionFlags(sphereBody->getCollisionFlags() | btCollisionObject::CF_CUSTOM_MATERIAL_CALLBACK);
	}
	void addCube(float xSize, float ySize, float zSize, float x, float y, float z, float mass) {
		//Position of the dynamic object (TODO � ajouter dans le object.h, c'est juste un test ici)
		btTransform t;
		t.setIdentity();
		t.setOrigin(btVector3(x, y, z));


		btBoxShape* cubeShape = new btBoxShape(btVector3(xSize, ySize, zSize)); // milieu longueur de table
		btMotionState* cubeMotion = new btDefaultMotionState(t);
		btRigidBody::btRigidBodyConstructionInfo info(
			mass,							//Mass
			cubeMotion,						//MotionState
			cubeShape,						//MotionState
			btVector3(0.0, 0.0, 0.0)		//Inertia
		);
		btRigidBody* cubeBody = new btRigidBody(info);
		dynamicsWorld->addRigidBody(cubeBody);
		cubeBody->setUserIndex(bodies.size());
		bodies.push_back(new bulletObject(cubeBody, bodies.size(), 0.0, 0.0, 0.0));
	}

	void addPlane(float xPos, float yPos, float zPos, float x, float y, float z) { // Normal as input
		btTransform t;
		t.setIdentity();
		t.setOrigin(btVector3(0.0, 0.0, 0.0));
		btStaticPlaneShape* groundShape = new btStaticPlaneShape(btVector3(x, y, z), 0.0);
		btMotionState* groundMotion = new btDefaultMotionState(t);
		btRigidBody::btRigidBodyConstructionInfo info2(
			0.0,							//Mass
			groundMotion,					//MotionState
			groundShape,					//MotionState
			btVector3(0.0, 0.0, 0.0)		//Inertia
		);
		btRigidBody* groundBody = new btRigidBody(info2);
		dynamicsWorld->addRigidBody(groundBody);
		groundBody->setUserIndex(bodies.size());
		bodies.push_back(new bulletObject(groundBody, bodies.size(), 0.0, 0.0, 0.0));
	}



	void addTable(Object planeTable) {
		//Position of the dynamic object (TODO � ajouter dans le object.h, c'est juste un test ici)
		btTransform t;
		t.setIdentity();
		t.setOrigin(btVector3(planeTable.model[3][0], planeTable.model[3][1], planeTable.model[3][2]));


		btBoxShape* tableBox1 = new btBoxShape(btVector3(1.9, 0.05, 0.75)); // milieu longueur de table
		btBoxShape* tableBox2 = new btBoxShape(btVector3(1.625, 0.05, 0.125)); //2�me partie de table between 2 holes
		btBoxShape* tableBoxHole = new btBoxShape(btVector3(0.05, 0.01, 0.05)); //hole
		btBoxShape* tableBox4 = new btBoxShape(btVector3(1.75, 0.2, 0.08)); // Bord longueur table
		btBoxShape* tableBox5 = new btBoxShape(btVector3(0.08, 0.2, 1.0)); // bord largeur de table

		btCompoundShape* tableShape = new btCompoundShape();
		//Plan table
		t.setOrigin(btVector3(planeTable.model[3][0], planeTable.model[3][1] / 2.0, planeTable.model[3][2]));
		tableShape->addChildShape(t, tableBox1);
		t.setOrigin(btVector3(planeTable.model[3][0], planeTable.model[3][1] / 2.0, planeTable.model[3][2] + 0.8));
		tableShape->addChildShape(t, tableBox2);
		t.setOrigin(btVector3(planeTable.model[3][0], planeTable.model[3][1] / 2.0, planeTable.model[3][2] - 0.8));
		tableShape->addChildShape(t, tableBox2);

		//Trous
		t.setOrigin(btVector3(planeTable.model[3][0] + 1.7, planeTable.model[3][1] / 2.0 - 0.03, planeTable.model[3][2] + 0.8));
		tableShape->addChildShape(t, tableBoxHole);
		t.setOrigin(btVector3(planeTable.model[3][0] - 1.7, planeTable.model[3][1] / 2.0 - 0.03, planeTable.model[3][2] + 0.8));
		tableShape->addChildShape(t, tableBoxHole);
		t.setOrigin(btVector3(planeTable.model[3][0] + 1.7, planeTable.model[3][1] / 2.0 - 0.03, planeTable.model[3][2] - 0.8));
		tableShape->addChildShape(t, tableBoxHole);
		t.setOrigin(btVector3(planeTable.model[3][0] - 1.7, planeTable.model[3][1] / 2.0 - 0.03, planeTable.model[3][2] - 0.8));
		tableShape->addChildShape(t, tableBoxHole);

		//Bords
		t.setOrigin(btVector3(planeTable.model[3][0], planeTable.model[3][1] / 2.0, planeTable.model[3][2] + 0.95));
		tableShape->addChildShape(t, tableBox4);
		t.setOrigin(btVector3(planeTable.model[3][0], planeTable.model[3][1] / 2.0, planeTable.model[3][2] - 0.95));
		tableShape->addChildShape(t, tableBox4);
		t.setOrigin(btVector3(planeTable.model[3][0] + 1.825, planeTable.model[3][1] / 2.0, planeTable.model[3][2]));
		tableShape->addChildShape(t, tableBox5);
		t.setOrigin(btVector3(planeTable.model[3][0] - 1.825, planeTable.model[3][1] / 2.0, planeTable.model[3][2]));
		tableShape->addChildShape(t, tableBox5);

		t.setOrigin(btVector3(planeTable.model[3][0], planeTable.model[3][1] / 2.0, planeTable.model[3][2]));

		btMotionState* tableMotion = new btDefaultMotionState(t);
		btRigidBody::btRigidBodyConstructionInfo info(
			0.0,							//Mass
			tableMotion,					//MotionState
			tableShape,						//MotionState
			btVector3(0.0, 0.0, 0.0)		//Inertia
		);
		btRigidBody* tableBody = new btRigidBody(info);
		dynamicsWorld->addRigidBody(tableBody);
		tableBody->setUserIndex(bodies.size());
		bodies.push_back(new bulletObject(tableBody, bodies.size(), 0.0, 0.0, 0.0));
	}
};
#endif