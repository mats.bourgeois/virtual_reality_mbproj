﻿#ifndef GAMEOBJECTS_H
#define GAMEOBJECTS_H

// CMakeProject1.h : fichier Include pour les fichiers Include système standard,
// ou les fichiers Include spécifiques aux projets.

#pragma once

#include <iostream>

#include <vector>
#include "object.h"
#include "shader.h"
#include "camera.h"
#include "texture.h"
#include "WorldPhys.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include<glm/gtc/matrix_transform.hpp>

#include<glm/gtc/type_ptr.hpp>
#include<glm/gtc/matrix_inverse.hpp>

struct Light {
	glm::vec3 Position;
	glm::vec3 Strength;
	glm::vec3 Attenuation;
	glm::vec3 Color;
	glm::vec4 misc; // [0] := lightType, [1] = direction if necessary
};

class GameObjects
{
public:
	std::vector<Light> lights;

	//Objects
	std::vector<Object> groundAndWalls;
	std::vector<Object> cubes;
	std::vector<Object> spheres;
	std::vector<Object> table; //0 = planeTable, 1 = table
	Object cubeMap;
	//std::vector<Shader> shaders;
	Shader defaultShader;
	Shader cubeMapShader;
	Shader lightShader;
	std::vector<Texture> textures;
	CubeMapTexture cubeMapTexture;

	WorldPhys dynWorld;

	Camera camera;
	//CAMERA SETUP
	glm::mat4 view;
	glm::mat4 perspective; 


	std::string pathSphere;
	std::string pathCube;
	std::string pathPlane;
	std::string pathPlaneTable;
	std::string pathTable;
	const char* objPath;
	const char* texPath;
	const char* shadPath;
	//Others
	int space_pressed = 0; //to avoid sphere spam

	GameObjects() {

		camera(glm::vec3(0.0, 1.8, 0.1));

		//CAMERA SETUP
		view = camera.GetViewMatrix();
		perspective = camera.GetProjectionMatrix();

		dynWorld();

		pathSphere = objPath "/sphere.obj";
		pathCube = objPath "/cube.obj";
		pathPlane = objPath "/plane.obj";
		pathPlaneTable = objPath "/planeTable3.obj";
		pathTable = objPath "/table.obj";
		
		// Shaders setup
		addShaders(shadPath);
		//Objects setup
		addObjects();
		// Textures setup
		addTextures();
		// Lights setup
		addLights();
		sendLights();
		// Physical world setup
		dynWorld.addTable(table.at(0));
		dynWorld.addPlane(0.0, 0.0, 0.0, 0.0, 0.1, 0.0);


	}

	void render() {

		view = camera.GetViewMatrix();
		// Table
		lightShader.use();
		lightShader.setMatrix4("V", view);
		lightShader.setMatrix4("P", perspective);

		defaultShader.use();
		defaultShader.setMatrix4("V", view);
		defaultShader.setMatrix4("P", perspective);
		defaultShader.setVector3f("u_view_pos", camera.Position);

		renderTable();
		// CubeMap
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMapText.ID);
		cubeMapShader.setInteger("cubemapTexture", 0);

		glDepthFunc(GL_LEQUAL);
		cubeMapShader.use();
		cubeMapShader.setMatrix4("V", view);
		cubeMapShader.setMatrix4("P", perspective);
		cubeMap.draw();
		glDepthFunc(GL_LESS);

		// Ground and walls
		for (Object gr : groundAndWalls) {
			gr.shader.use();
			gr.shader.use();
		}

		// Cube
		for (Object cube : cubes) {
			cube.shader.use();
			if (cube.shader == lightShader) {
				lightShader.setMatrix4("M", cube.model);
				lightShader.setVector3f("color", lights.at(0).Color);
				cube.draw();
			}
			else {

			}
		}		

		// Sphere		
		btTransform tSphere;
		Object sphere = spheres.at(0);
		dynWorld.bodies.at(2)->getMotionState()->getWorldTransform(tSphere);
		float mat[16];
		tSphere.getOpenGLMatrix(mat);
		spheres.at(0).model = glm::make_mat4(mat);
		spheres.at(0).model = glm::scale(sphere1.model, glm::vec3(0.03));
		lights.at(1).Position = glm::vec3(spheres.at(0).model[3] + glm::vec4(0.0, 0.03, 0.0, 0.0));
		lightShader.setVector3f("color", lights.at(1).Color);
		lightShader.setMatrix4("M", spheres.at(0).model);
		glm::mat4 itM = glm::inverseTranspose(sphere.at(0).model);
		spheres.at(0).draw();

		defaultShader.use();
		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D, text3.ID);
		

		// Drawing of the spheres that need the shader1
		for (int i = 3; i < dynWorld.bodies.size(); i++) {
			sphere = spheres.at(i - 2);
			dynWorld.bodies.at(i)->getMotionState()->getWorldTransform(tSphere);
			tSphere.getOpenGLMatrix(mat);
			sphere.model = glm::make_mat4(mat);
			sphere.model = glm::scale(sphere.model, glm::vec3(0.03));
			shader.setMatrix4("M", sphere.model);
			glm::mat4 itM = glm::inverseTranspose(sphere.model);
			shader.setMatrix4("itM", itM);
			sphere.draw();
		}

	}


	void addShaders(std::string shadPath) {

		const std::string sourceV = shadPath "/Vertex.vert";
		const std::string sourceF = shadPath "/Fragment.frag";
		defaultShader(sourceV.c_str(), sourceF.c_str());

		const std::string sourceVCubeMap = shadPath "/cubeMap.vert";
		const std::string sourceFCubeMap = shadPath "/cubeMap.frag";
		cubeMapShader = Shader(sourceVCubeMap.c_str(), sourceFCubeMap.c_str());

		const std::string sourceVLight = shadPath "/light.vert";
		const std::string sourceFLight = shadPath "/light.frag";
		lightShader(sourceVLight.c_str(), sourceFLight.c_str());
	}

	void addObjects() {
		addCubeMap(cubeMapShader);
		addTable();
		addGround(glm::vec3(4.0, 0.0, 4.0));
		addGround(glm::vec3(4.0, 0.0, -4.0));
		addGround(glm::vec3(-4.0, 0.0, 4.0));
		addGround(glm::vec3(-4.0, 0.0, -4.0));
		
		addSphere(lightShader, glm::(1.0, 0.6, 0.0));
		addSphere(glm::(-1.0, 0.6, 0.0));
		addSphere(glm::(0.0, 0.6, 0.0));
		addCube(lightShader, glm::vec3(5.0, 5.0, 0.0), glm::vec3(0.45, 0.45, 0.45));
	}
	
	void addSphere(Shader shader=defaultShader, glm::vec3 position) {
		Object sphere(pathSphere.c_str());
		sphere.makeObject(shader);
		sphere.model = glm::translate(sphere.model, position);
		sphere.model = glm::scale(sphere.model, glm::vec3(0.015, 0.015, 0.015));
		spheres.push_back(sphere);
		dynWorld.addSphere(0.03, sphere.model[3][0], sphere.model[3][1] * 2.0, sphere.model[3][2], 0.2);
	}

	void addTable(Shader shader=defaultShader) {
		Object planeTable(pathPlaneTable.c_str());
		planeTable.makeObject(shader);
		planeTable.model = glm::translate(planeTable.model, glm::vec3(0.0, 1.0, 0.0));
		table.push_back(planeTable);

		Object table(pathTable.c_str());
		table.makeObject(shader);
		table.push_back(table);
	}

	void addGround(Shader shader=defaultShader, glm::vec3 position) {
		Object ground(pathCube.c_str());
		ground.makeObject(shader);
		ground.model = glm::translate(ground1.model, position);
		ground.model = glm::scale(ground1.model, glm::vec3(4.0, 0.001, 4.0));
		groundAndWalls.push_back(ground);
	}

	void addCubeMap() {
		Object cubeMap(pathCube.c_str());
		cubeMap.makeObject(cubeMapShader);
		this->cubeMap = cubeMap;
	}

	void addCube(Shader shader = defaultShader, glm::vec3 position, glm::vec3 scale) {
		Object cube(pathCube.c_str());
		cube.makeObject(shader);
		cube.model = glm::translate(cube.model, position);
		cube.model = glm::scale(cube.model, scale);
	}

	void addTextures() {
		//Rendering
		//CubeMap settings
		std::string pathToCubeMap = texPath "/cubemaps/sky/";
		CubeMapTexture cubeMapText(pathToCubeMap, 0);

		// Other textures setup
		std::string path = texPath "/wood.png";
		Texture text1(path.c_str(), 1);
		path = texPath "/snooker_Texture.jpg";
		Texture text2(path.c_str(), 2);
		path = texPath "/planks.png";
		Texture text3(path.c_str(), 2);
	}

	void renderTable() {
		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D, text3.ID);
		defaultShader.setInteger("u_text", 3);

		defaultShader.setMatrix4("M", table.model);
		defaultShader.setMatrix4("itM", glm::inverseTranspose(table.model));
		table.draw();

		// Drawing the plane of the table
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, text2.ID);

		defaultShader.setInteger("u_text", 2);
		defaultShader.setMatrix4("M", planeTable.model);
		defaultShader.setMatrix4("itM", glm::inverseTranspose(planeTable.model));
		planeTable.draw();

	}

	void addLights(){
		Light l1 = {
		glm::vec3(cubes.at[0].model[3]) ,//Position
		glm::vec3(0.1, 0.6, 1.6),//Strength
		glm::vec3(0.2, 0.01, 0.05),//Att
		glm::vec3(1.0, 1.0, 1.0),//Color
		glm::vec4(2.0, 0.0, 1.0, 0.0) //misc
		};
		Light l2 = {
			glm::vec3(spheres.at[0].model[3]),//Position
		glm::vec3(0.0, 0.5, 0.8),//Strength
		glm::vec3(1.0, 0.01, 0.05),//Att
		glm::vec3(0.8, 0.0, 0.3),//Color
		glm::vec4(0.0)				//misc
		};
		Light l3 = {
		glm::vec3(1.5,2.0, 5.0),//Position
		glm::vec3(0.0, 0.0, 0.0),//Strength
		glm::vec3(1.0, 0.2, 0.07),//Att
		glm::vec3(0.6, 0.8, 0.0),//Color
		glm::vec4(0.0)				//misc
		};
		Light l4 = {
		glm::vec3(1.0,3.0, -5.0),//Position
		glm::vec3(0.0, 0.0, 0.0),//Strength
		glm::vec3(1.0, 0.01, 0.05),//Att
		glm::vec3(0.9, 0.6, 0.6),//Color
		glm::vec4(0.0)				//misc
		};
		this->lights.push_back(l1);
		this->lights.push_back(l2);
		this->lights.push_back(l3);
		this->lights.push_back(l4);

	}

	void sendLights(Shader shader = defaultShader) {
		shader.use();

		shader.setVector3f("materialColour", materialColour);
		shader.setFloat("shininess", shininess);
		//LIGHT SENDING TO SHADER
		for (int i = 0; i < lights.size(); i++) {

			shader.setVector3f(("lights[" + std::to_string(i) + "].pos").c_str(), lights[i].Position);
			shader.setVector3f(("lights[" + std::to_string(i) + "].strength").c_str(), lights[i].Strength);
			shader.setVector3f(("lights[" + std::to_string(i) + "].attenuation").c_str(), lights[i].Attenuation);
			shader.setVector3f(("lights[" + std::to_string(i) + "].color").c_str(), lights[i].Color);
			shader.setVector3f(("lights[" + std::to_string(i) + "].misc").c_str(), lights[i].misc);
		}
	}

	void processInput(GLFWwindow* window) {

		if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
			glfwSetWindowShouldClose(window, true);

		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
			camera.ProcessKeyboardMovement(LEFT, 0.1);
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
			camera.ProcessKeyboardMovement(RIGHT, 0.1);

		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
			camera.ProcessKeyboardMovement(FORWARD, 0.1);
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
			camera.ProcessKeyboardMovement(BACKWARD, 0.1);

		if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
			camera.ProcessKeyboardRotation(1, 0.0, 1);
		if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
			camera.ProcessKeyboardRotation(-1, 0.0, 1);

		if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
			camera.ProcessKeyboardRotation(0.0, 1.0, 1);
		if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
			camera.ProcessKeyboardRotation(0.0, -1.0, 1);
		if (glfwGetWindowAttrib(window, GLFW_HOVERED))
		{
			double xpos, ypos;
			//getting cursor position
			glfwGetCursorPos(window, &xpos, &ypos);
			mouse_callback(window, xpos, ypos);
		}

		if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
			if (space_pressed == 0)
			{
				space_pressed = 1;
				this->dynWorld.addSphere(0.03, camera.Position.x, camera.Position.y, camera.Position.z, 0.2);
				glm::vec3 look = camera.Front;
				dynWorld.bodies[dynWorld.bodies.size()-1]->setLinearVelocity(btVector3(look.x * 5.0, look.y * 5.0, look.z * 5.0));
				addSphere(defaultShader, camera.Position);
			}
		}
		if (glfwGetKey(window, GLFW_KEY_SPACE) != GLFW_PRESS)
			space_pressed = 0;

	}
};

#endif