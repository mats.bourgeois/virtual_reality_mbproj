#ifndef TEXTURE_H
#define TEXTURE_H

#include<iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>


#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include<glm/gtc/matrix_transform.hpp>


class Texture
{
public:
	int imWidth, imHeight, imNrChannels;
	GLuint textInd;
	GLuint ID;
	std::string type;


	Texture(const char* path, GLuint textureInd, std::string type ="") {

		this->textInd = textureInd;
		this->type = type;
		glGenTextures(1, &ID);
		glActiveTexture(GL_TEXTURE0 + textureInd);
		glBindTexture(GL_TEXTURE_2D, ID);


		// We need to flip the image so it appears right side up
		stbi_set_flip_vertically_on_load(true);

		unsigned char* data = stbi_load(path, &imWidth, &imHeight, &imNrChannels, 0);

		// Define the parameters for the texture
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);


		if (data)
		{	// Check what type of color channels the texture has and load it accordingly
			if (type == "normal") {

				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imWidth, imHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
			}
			else if (imNrChannels == 4)
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, imWidth, imHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
			else if (imNrChannels == 3)
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, imWidth, imHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
			else if (imNrChannels == 1)
				glTexImage2D(GL_TEXTURE_2D,0, GL_RGBA, imWidth, imHeight, 0, GL_RED, GL_UNSIGNED_BYTE, data);
			else
				throw std::invalid_argument("Automatic Texture type recognition failed");
			glGenerateMipmap(GL_TEXTURE_2D);
		}
		else {
			std::cout << "Failed to Load texture" << textureInd << std::endl;
			const char* reason = stbi_failure_reason();
			std::cout << reason << std::endl;
		}

		stbi_image_free(data);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
};

class CubeMapTexture {
public:
	int imWidth, imHeight, imNrChannels;
	GLuint textInd;
	GLuint ID;

	CubeMapTexture(std::string pathToCubeMap, GLuint textureInd){
		this->textInd = textureInd;
		glGenTextures(1, &ID);
		glActiveTexture(GL_TEXTURE0 + textureInd);
		glBindTexture(GL_TEXTURE_CUBE_MAP, ID);

		// texture parameters
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		//stbi_set_flip_vertically_on_load(true);


		std::map<std::string, GLenum> facesToLoad = {
			{pathToCubeMap + "right.jpg",GL_TEXTURE_CUBE_MAP_POSITIVE_X},
			{pathToCubeMap + "top.jpg",GL_TEXTURE_CUBE_MAP_POSITIVE_Y},
			{pathToCubeMap + "front.jpg",GL_TEXTURE_CUBE_MAP_POSITIVE_Z},
			{pathToCubeMap + "left.jpg",GL_TEXTURE_CUBE_MAP_NEGATIVE_X},
			{pathToCubeMap + "bottom.jpg",GL_TEXTURE_CUBE_MAP_NEGATIVE_Y},
			{pathToCubeMap + "back.jpg",GL_TEXTURE_CUBE_MAP_NEGATIVE_Z},
		};
		//load the six faces, you need to complete the function
		for (std::pair<std::string, GLenum> pair : facesToLoad) {
			loadCubemapFace(pair.first.c_str(), pair.second);
		}
	}
private:
	void loadCubemapFace(const char* path, const GLenum& targetFace)
	{
		unsigned char* data = stbi_load(path, &imWidth, &imHeight, &imNrChannels, 0);
		if (data)
		{

			glTexImage2D(targetFace, 0, GL_RGB, imWidth, imHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
			//glGenerateMipmap(targetFace);
		}
		else {
			std::cout << "Failed to Load texture CubeMap" << std::endl;
			const char* reason = stbi_failure_reason();
			std::cout << reason << std::endl;
		}
		stbi_image_free(data);
	}

};
#endif