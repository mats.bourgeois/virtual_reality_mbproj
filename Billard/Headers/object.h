#ifndef OBJECT_H
#define OBJECT_H

#include<iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

#include "shader.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include<glm/gtc/matrix_transform.hpp>

struct Vertex {
	glm::vec3 Position;
	glm::vec2 Texture;
	glm::vec3 Normal;
};

class Object
{
public:
	std::vector<glm::vec3> positions;
	std::vector<glm::vec2> textures;
	std::vector<glm::vec3> normals;
	std::vector<Vertex> vertices;

	int numVertices;

	GLuint VBO, VAO;

	glm::mat4 model = glm::mat4(1.0);


	Object(const char* path) {

		// Read the file defined by the path argument 
		std::ifstream myfile(path);
		// open the .obj file into a text editor and see how the data are organized
		// you will see line starting by v, vt, vn and f --> what are these ? 
		// v = vertex coord, vt = texture coord, vn = normal vectors, 
		// f = face -> vertex_index/texture_index/normal_index
		std::string line;
		numVertices = 0;
		if (myfile.is_open()) {
			// Then parse it to extract the data you need
			while (std::getline(myfile, line)) {
				std::istringstream iss(line);
				std::string index;
				iss >> index;
				//std::cout << "indice : " << indice << std::endl;
				if (index == "v") {
					float x, y, z;
					iss >> x >> y >> z;
					positions.push_back(glm::vec3(x, y, z));
				}
				else if (index == "vt") {
					float textX, textY;
					iss >> textX >> textY;
					textures.push_back(glm::vec2(textX, textY));
				}
				else if (index == "vn") {
					float nX, nY, nZ;
					iss >> nX >> nY >> nZ;
					normals.push_back(glm::vec3(nX, nY, nZ));
				}
				else if (index == "f") {
					std::string f1, f2, f3;
					iss >> f1 >> f2 >> f3;
					std::vector<std::string> f;
					f.push_back(f1);
					f.push_back(f2);
					f.push_back(f3);


					std::string p, t, n;

					//for face 1
					Vertex v1;

					p = f1.substr(0, f1.find("/"));
					f1.erase(0, f1.find("/") + 1);

					t = f1.substr(0, f1.find("/"));
					f1.erase(0, f1.find("/") + 1);

					n = f1.substr(0, f1.find("/"));


					v1.Position = positions.at(std::stof(p) - 1);
					v1.Normal = normals.at(std::stof(n) - 1);
					v1.Texture = textures.at(std::stof(t) - 1);
					vertices.push_back(v1);

					//for face 12
					Vertex v2;

					p = f2.substr(0, f2.find("/"));
					f2.erase(0, f2.find("/") + 1);

					t = f2.substr(0, f2.find("/"));
					f2.erase(0, f2.find("/") + 1);

					n = f2.substr(0, f2.find("/"));


					v2.Position = positions.at(std::stof(p) - 1);
					v2.Normal = normals.at(std::stof(n) - 1);
					v2.Texture = textures.at(std::stof(t) - 1);
					vertices.push_back(v2);

					//for face 3
					Vertex v3;

					p = f3.substr(0, f3.find("/"));
					f3.erase(0, f3.find("/") + 1);

					t = f3.substr(0, f3.find("/"));
					f3.erase(0, f3.find("/") + 1);

					n = f3.substr(0, f3.find("/"));


					v3.Position = positions.at(std::stof(p) - 1);
					v3.Normal = normals.at(std::stof(n) - 1);
					v3.Texture = textures.at(std::stof(t) - 1);
					vertices.push_back(v3);
				}
			}
			// keep track of the number of vertices you need
			std::cout << "Load model with " << vertices.size() << " vertices" << std::endl;
		}
		myfile.close();
		numVertices = vertices.size();

	}



	void makeObject(Shader shader, bool texture = true) {
		float* data = new float[8 * numVertices];
		for (int i = 0; i < numVertices; i++) {
			Vertex v = vertices.at(i);
			data[i * 8] = v.Position.x;
			data[i * 8+1] = v.Position.y;
			data[i * 8+2] = v.Position.z;

			data[i * 8+3] = v.Texture.x*8.0;
			data[i * 8+4] = v.Texture.y*8.0;

			data[i * 8+5] = v.Normal.x;
			data[i * 8+6] = v.Normal.y;
			data[i * 8+7] = v.Normal.z;
		}
		//Create the VAO and VBO
		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO);
		//Put your data into your VBO
		glBindVertexArray(VAO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * numVertices, data, GL_STATIC_DRAW);

		//Define VBO and VAO as active buffer and active vertex array		
		//Use the VAO to specify how your data should be read by your shader (glVertexAttribPointer and co)
		//Sometimes your shader will not use texture or normal attribute
		//you can use the boolean defined above to not specify these attribute 
		auto att_pos = glGetAttribLocation(shader.ID, "position");
		glEnableVertexAttribArray(att_pos);
		glVertexAttribPointer(att_pos, 3, GL_FLOAT, false, 8 * sizeof(float), (void*)0);

		if (texture) {
			auto att_text = glGetAttribLocation(shader.ID, "tex_coords");
			glEnableVertexAttribArray(att_text);
			glVertexAttribPointer(att_text, 2, GL_FLOAT, false, 8 * sizeof(float), (void*)(3 * sizeof(float)));
		}
		auto att_normal = glGetAttribLocation(shader.ID, "normal");
		glEnableVertexAttribArray(att_normal);
		glVertexAttribPointer(att_normal, 3, GL_FLOAT, false, 8 * sizeof(float), (void*)(5 * sizeof(float)));

		

		//desactive the buffer and delete the datas when your done
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);

		delete[] data;

	}

	

	void draw() {

		//bind your vertex arrays and call glDrawArrays
		glBindVertexArray(this->VAO);
		glDrawArrays(GL_TRIANGLES, 0, numVertices);
	}

	void draw(Shader shadowMap) {

		glBindVertexArray(VAO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		
		auto att_pos = glGetAttribLocation(shadowMap.ID, "position");
		glEnableVertexAttribArray(att_pos);
		glVertexAttribPointer(att_pos, 3, GL_FLOAT, false, 8 * sizeof(float), (void*)0);


		//desactive the buffer and delete the datas when your done
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
		//bind your vertex arrays and call glDrawArrays
		glBindVertexArray(this->VAO);
		glDrawArrays(GL_TRIANGLES, 0, numVertices);
	}


};
#endif