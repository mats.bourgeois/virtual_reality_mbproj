﻿#include<iostream>

//include glad before GLFW to avoid header conflict or define "#define GLFW_INCLUDE_NONE"
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include<glm/gtc/matrix_transform.hpp>

#include<glm/gtc/type_ptr.hpp>
#include<glm/gtc/matrix_inverse.hpp>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include <map>

#include "camera.h"
#include "shader.h"
#include "object.h"
#include "texture.h"
#include "WorldPhys.h"

#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

#include <btBulletCollisionCommon.h>
#include <btBulletDynamicsCommon.h>


const int width = 1600;
const int height = 900;
const float camScale = height / width;


void processInput(GLFWwindow* window);
void loadCubemapFace(const char* file, const GLenum& targetCube);
void renderQuad();

void mouse_callback(GLFWwindow* window, double xposIn, double yposIn);
float lastX = width / 2.0f;
float lastY = height / 2.0f;
bool firstMouse = true;


WorldPhys dynWorld;


#ifndef NDEBUG
void APIENTRY glDebugOutput(GLenum source,
	GLenum type,
	unsigned int id,
	GLenum severity,
	GLsizei length,
	const char* message,
	const void* userParam)
{
	// ignore non-significant error/warning codes
	if (id == 131169 || id == 131185 || id == 131218 || id == 131204) return;

	std::cout << "---------------" << std::endl;
	std::cout << "Debug message (" << id << "): " << message << std::endl;

	switch (source)
	{
	case GL_DEBUG_SOURCE_API:             std::cout << "Source: API"; break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:   std::cout << "Source: Window System"; break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER: std::cout << "Source: Shader Compiler"; break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:     std::cout << "Source: Third Party"; break;
	case GL_DEBUG_SOURCE_APPLICATION:     std::cout << "Source: Application"; break;
	case GL_DEBUG_SOURCE_OTHER:           std::cout << "Source: Other"; break;
	} std::cout << std::endl;

	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR:               std::cout << "Type: Error"; break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: std::cout << "Type: Deprecated Behaviour"; break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  std::cout << "Type: Undefined Behaviour"; break;
	case GL_DEBUG_TYPE_PORTABILITY:         std::cout << "Type: Portability"; break;
	case GL_DEBUG_TYPE_PERFORMANCE:         std::cout << "Type: Performance"; break;
	case GL_DEBUG_TYPE_MARKER:              std::cout << "Type: Marker"; break;
	case GL_DEBUG_TYPE_PUSH_GROUP:          std::cout << "Type: Push Group"; break;
	case GL_DEBUG_TYPE_POP_GROUP:           std::cout << "Type: Pop Group"; break;
	case GL_DEBUG_TYPE_OTHER:               std::cout << "Type: Other"; break;
	} std::cout << std::endl;

	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:         std::cout << "Severity: high"; break;
	case GL_DEBUG_SEVERITY_MEDIUM:       std::cout << "Severity: medium"; break;
	case GL_DEBUG_SEVERITY_LOW:          std::cout << "Severity: low"; break;
	case GL_DEBUG_SEVERITY_NOTIFICATION: std::cout << "Severity: notification"; break;
	} std::cout << std::endl;
	std::cout << std::endl;
}
#endif

Camera camera(glm::vec3(0.0, 30.0, 0.0));
/*btDiscreteDynamicsWorld* dynamicsWorld;
std::vector<btRigidBody*> bodies;*/


int coll[2];


bool callbackFunc(btManifoldPoint& cp, const btCollisionObjectWrapper* obj1, int id1, int index1, const btCollisionObjectWrapper* obj2, int id2, int index2) {
	//std::cout << ((bulletObject*)obj1->getCollisionObject()->getUserPointer())->id << " " << ((bulletObject*)obj2->getCollisionObject()->getUserPointer())->id << std::endl;
	coll[0] = obj1->getCollisionObject()->getUserIndex();
	coll[1] = obj2->getCollisionObject()->getUserIndex();

	return false;
}



int main(int argc, char* argv[])
{
	std::cout << "Welcome to exercice 7: " << std::endl;
	std::cout << "Complete light equation and attenuation factor\n"
		"Implement the complete light equation.\n"
		"Make the light move with time closer and further of the model.\n"
		"Put the attenuation factor into the calculations\n"
		"\n";


	//Boilerplate
	//Create the OpenGL context 
	if (!glfwInit()) {
		throw std::runtime_error("Failed to initialise GLFW \n");
	}
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifndef NDEBUG
	//create a debug context to help with Debugging
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);
#endif


	//Create the window
	GLFWwindow* window = glfwCreateWindow(width, height, "Snooker?", nullptr, nullptr);
	if (window == NULL)
	{
		glfwTerminate();
		throw std::runtime_error("Failed to create GLFW window\n");
	}

	glfwMakeContextCurrent(window);

	//load openGL function
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		throw std::runtime_error("Failed to initialize GLAD");
	}

	//Z BUFFER enabling
	glEnable(GL_DEPTH_TEST);
	//Possible Face culling enabling
	/*
	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);
	glFrontFace(GL_CW);
	*/

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

#ifndef NDEBUG
	int flags;
	glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
	if (flags & GL_CONTEXT_FLAG_DEBUG_BIT)
	{
		glEnable(GL_DEBUG_OUTPUT);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageCallback(glDebugOutput, nullptr);
		glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
	}
#endif
	const std::string sourceV = PATH_TO_SHADER "/Vertex.vert";
	const std::string sourceF = PATH_TO_SHADER "/Fragment.frag";
	//Shader shader(sourceV.c_str(), sourceF.c_str());

	const std::string sourceVG = PATH_TO_SHADER "/vertGeom.vert";
	const std::string sourceFG = PATH_TO_SHADER "/fragGeom.frag";
	const std::string sourceG = PATH_TO_SHADER "/Geometry.geom";
	Shader shader(sourceVG.c_str(), sourceFG.c_str(), sourceG.c_str());

	const std::string sourceVShadowCubeMap = PATH_TO_SHADER "/shadowCubeMap.vert";
	const std::string sourceFShadowCubeMap = PATH_TO_SHADER "/shadowCubeMap.frag";
	const std::string sourceGShadowCubeMap = PATH_TO_SHADER "/shadowCubeMap.geom";
	Shader shadowCubeMap(sourceVShadowCubeMap.c_str(), sourceFShadowCubeMap.c_str(), sourceGShadowCubeMap.c_str());


	const std::string sourceVLight = PATH_TO_SHADER "/light.vert";
	const std::string sourceFLight = PATH_TO_SHADER "/light.frag";
	Shader lightShader(sourceVLight.c_str(), sourceFLight.c_str());



	const std::string sourceVCubeMap = PATH_TO_SHADER "/cubeMap.vert";
	const std::string sourceFCubeMap = PATH_TO_SHADER "/cubeMap.frag";
	Shader cubeMapShader = Shader(sourceVCubeMap.c_str(), sourceFCubeMap.c_str());

	const std::string sourceVshadowMap = PATH_TO_SHADER "/shadowMap.vert";
	const std::string sourceFshadowMap = PATH_TO_SHADER "/shadowMap.frag";
	Shader shadowMapShader = Shader(sourceVshadowMap.c_str(), sourceFshadowMap.c_str());
	const std::string sourceVshadowRend = PATH_TO_SHADER "/shadowRender.vert";
	const std::string sourceFshadowRend = PATH_TO_SHADER "/shadowRender.frag";
	Shader shadowRender = Shader(sourceVshadowRend.c_str(), sourceFshadowRend.c_str());


	struct Light {
		glm::vec3 Position;
		glm::vec3 Strength;
		glm::vec3 Attenuation;
		glm::vec3 Color;
		glm::vec4 misc; // [0] := lightType, [1] = direction if necessary
	};



	std::string pathSphere = PATH_TO_OBJECTS "/sphere.obj";
	std::string pathCube = PATH_TO_OBJECTS "/cube.obj";
	std::string pathPlane = PATH_TO_OBJECTS "/plane.obj";
	std::string pathPlaneTable = PATH_TO_OBJECTS "/planeTable3.obj";
	std::string pathTable = PATH_TO_OBJECTS "/table.obj";


	// Objects list TODO: make gameCreator which will have a vector of every initial object in the game
	Object sphere1(pathSphere.c_str());
	sphere1.makeObject(lightShader);
	sphere1.model = glm::translate(sphere1.model, glm::vec3(1.0, 0.6, 0.0));
	sphere1.model = glm::scale(sphere1.model, glm::vec3(0.015, 0.015, 0.015));

	Object sphere2(pathSphere.c_str());
	sphere2.makeObject(shader);
	sphere2.model = glm::translate(sphere2.model, glm::vec3(-1.0, 0.6, 0.0));
	sphere2.model = glm::scale(sphere2.model, glm::vec3(0.015, 0.015, 0.015));

	glm::vec3 spotLightPos = glm::vec3(0.0, 10.0, 0.0);

	Object lightSphere(pathSphere.c_str());
	lightSphere.makeObject(shader);
	lightSphere.model = glm::translate(lightSphere.model, spotLightPos);
	lightSphere.model = glm::scale(lightSphere.model, glm::vec3(0.2, 0.2, 0.2));

	Object planeTable(pathPlaneTable.c_str());
	planeTable.makeObject(shader);
	planeTable.model = glm::translate(planeTable.model, glm::vec3(0.0, 1.0, 0.0));

	Object table(pathTable.c_str());
	table.makeObject(shader);

	std::vector<Object> grounds;
	Object ground1(pathPlane.c_str());
	ground1.makeObject(shader);
	ground1.model = glm::translate(ground1.model, glm::vec3(4.0, 0.0, 4.0));
	ground1.model = glm::scale(ground1.model, glm::vec3(4.0, 0.001, 4.0));
	grounds.push_back(ground1);

	Object ground2(pathPlane.c_str());
	ground2.makeObject(shader);
	ground2.model = glm::translate(ground2.model, glm::vec3(-4.0, 0.0, 4.0));
	ground2.model = glm::scale(ground2.model, glm::vec3(4.0, 0.001, 4.0));
	grounds.push_back(ground2);

	Object ground3(pathPlane.c_str());
	ground3.makeObject(shader);
	ground3.model = glm::translate(ground3.model, glm::vec3(4.0, 0.0, -4.0));
	ground3.model = glm::scale(ground3.model, glm::vec3(4.0, 0.001, 4.0));
	grounds.push_back(ground3);

	Object ground4(pathPlane.c_str());
	ground4.makeObject(shader);
	ground4.model = glm::translate(ground4.model, glm::vec3(-4.0, 0.0, -4.0));
	ground4.model = glm::scale(ground4.model, glm::vec3(4.0, 0.001, 4.0));
	grounds.push_back(ground4);

	std::vector<Object> walls;
	Object wall1(pathCube.c_str());
	wall1.makeObject(shader);
	wall1.model = glm::translate(wall1.model, glm::vec3(0.0, 2.0, 8.0));
	wall1.model = glm::rotate(wall1.model, glm::radians(90.0f), glm::vec3(0.0, 0.0, 1.0));
	wall1.model = glm::scale(wall1.model, glm::vec3(4.0, 8.0, 0.25));
	walls.push_back(wall1);

	Object wall2(pathCube.c_str());
	wall2.makeObject(shader);
	wall2.model = glm::translate(wall2.model, glm::vec3(0.0, 2.0, -8.0));
	wall2.model = glm::rotate(wall2.model, glm::radians(90.0f), glm::vec3(0.0, 0.0, 1.0));
	wall2.model = glm::scale(wall2.model, glm::vec3(4.0, 8.0, 0.25));
	walls.push_back(wall2);
	Object wall3(pathCube.c_str());
	wall3.makeObject(shader);
	wall3.model = glm::translate(wall3.model, glm::vec3(8.0, 2.0, 0.0));
	wall3.model = glm::rotate(wall3.model, glm::radians(90.0f), glm::vec3(1.0, 0.0, 0.0));
	wall3.model = glm::scale(wall3.model, glm::vec3(0.25, 8.0, 4.0));
	walls.push_back(wall3);
	Object wall4(pathCube.c_str());
	wall4.makeObject(shader);
	wall4.model = glm::translate(wall4.model, glm::vec3(-8.0, 2.0, 0.0));
	wall4.model = glm::rotate(wall4.model, glm::radians(90.0f), glm::vec3(1.0, 0.0, 0.0));
	wall4.model = glm::scale(wall4.model, glm::vec3(0.25, 8.0, 4.0));
	walls.push_back(wall4);

	glm::vec3 lightPos = glm::vec3(-20.0, 30.0, -30.0);

	Object lightCube(pathSphere.c_str());
	lightCube.makeObject(lightShader);
	lightCube.model = glm::translate(lightCube.model, glm::vec3(lightPos));
	lightCube.model = glm::scale(lightCube.model, glm::vec3(1.0, 1.0, 1.0));

	glm::vec3 cubePos = glm::vec3(2.0, 2.0, 2.0);

	Object cube(pathCube.c_str());
	cube.makeObject(shader);
	cube.model = glm::translate(cube.model, cubePos);
	cube.model = glm::scale(cube.model, glm::vec3(0.2, 0.2, 0.2));

	Object cubeMap(pathCube.c_str());
	cubeMap.makeObject(cubeMapShader);




	double prev = 0;
	int deltaFrame = 0;
	//fps function
	auto fps = [&](double now) {
		double deltaTime = now - prev;
		deltaFrame++;
		if (deltaTime > 0.5) {
			prev = now;
			const double fpsCount = (double)deltaFrame / deltaTime;
			deltaFrame = 0;
			std::cout << "\r FPS: " << fpsCount;
			std::cout.flush();
		}
	};


	//CAMERA SETUP
	glm::mat4 view = camera.GetViewMatrix();
	glm::mat4 perspective = camera.GetProjectionMatrix();

	//LIGHT PARAMETERS
	glm::vec3 light_pos = glm::vec3(1.0, 2.0, 1.5);

	float ambient = 0.01;
	float diffuse = 0.5;
	float specular = 0.8;

	glm::vec3 materialColour = glm::vec3(0.926f, 0.766, 0.75);
	float shininess = 32.0;

	//Rendering
	//CubeMap settings
	std::string pathToCubeMap = PATH_TO_TEXTURE "/cubemaps/sky/";
	CubeMapTexture cubeMapText(pathToCubeMap, 0);

	// Other textures setup
	std::string path = PATH_TO_TEXTURE "/wood.png";
	Texture text1(path.c_str(), 1);
	path = PATH_TO_TEXTURE "woodNormal.png";
	Texture text2(path.c_str(), 2, "normal");
	path = PATH_TO_TEXTURE "/snooker_Texture.jpg";
	Texture text3(path.c_str(), 3);
	path = PATH_TO_TEXTURE "/snookerNormal.png";
	Texture text4(path.c_str(), 4, "normal");
	path = PATH_TO_TEXTURE "/planks.png";
	Texture text5(path.c_str(), 5);
	path = PATH_TO_TEXTURE "/planksNormal.png";
	Texture text6(path.c_str(), 6, "normal");
	path = PATH_TO_TEXTURE "/bricks.png";
	Texture text7(path.c_str(), 7);
	path = PATH_TO_TEXTURE "/bricksNormal.png";
	Texture text8(path.c_str(), 8, "normal");
	path = PATH_TO_TEXTURE "/brown.png";
	Texture text15(path.c_str(), 15);
	path = PATH_TO_TEXTURE "/brownNormal.png";
	Texture text16(path.c_str(), 16, "normal");
	path = PATH_TO_TEXTURE "/container.png";
	Texture text17(path.c_str(), 17);
	path = PATH_TO_TEXTURE "/containerNormal.png";
	Texture text18(path.c_str(), 18, "normal");

	shader.use();

	shader.setVector3f("materialColour", materialColour);
	shader.setFloat("shininess", shininess);

	// LIGHTS PARAMETERS
	std::vector<Light> lights;
	Light l1 = {
		glm::vec3(lightPos) ,//Position
		glm::vec3(0.2, 30.0, 18.2),//Strength
		glm::vec3(0.2, 0.025, 0.03),//Att
		glm::vec3(1.0, 1.0, 1.0),//Color
		glm::vec4(2.0,lightPos) //misc
	};
	shader.setVector3f("lightPos1", lightPos);
	Light l2 = {
		glm::vec3(sphere1.model[3]),//Position
		glm::vec3(0.0, 1.5, 0.9),//Strength
		glm::vec3(0.3, 0.01, 0.05),//Att
		glm::vec3(0.8, 0.0, 0.3),//Color
		glm::vec4(0.0)				//misc
	};
	shader.setVector3f("lightPos2", l2.Position);
	Light spotLight = {
		spotLightPos,//Position
		glm::vec3(0.0, 1.5, 1.2),//Strength
		glm::vec3(0.3, 0.2, 0.07),//Att
		glm::vec3(0.6, 0.8, 0.0),//Color
		glm::vec4(1.0, 0.0, 0.0, 0.0)				//misc
	};
	shader.setVector3f("lightPos3", spotLight.Position);
	Light l4 = {
		cubePos,//Position
		glm::vec3(0.0, 0.8, 0.5),//Strength
		glm::vec3(1.0, 0.01, 0.05),//Att
		glm::vec3(0.2, 0.6, 0.6),//Color
		glm::vec4(0.0)				//misc
	};
	shader.setVector3f("lightPos4", l4.Position);
	shader.setVector3f("lightPos5", l4.Position);
	lights.push_back(l1);
	lights.push_back(l2);
	lights.push_back(spotLight);
	lights.push_back(l4);

	//LIGHT SENDING TO SHADER
	for (int i = 0; i < lights.size(); i++) {

		shader.setVector3f(("lights[" + std::to_string(i) + "].pos").c_str(), lights[i].Position);
		shader.setVector3f(("lights[" + std::to_string(i) + "].strength").c_str(), lights[i].Strength);
		shader.setVector3f(("lights[" + std::to_string(i) + "].attenuation").c_str(), lights[i].Attenuation);
		shader.setVector3f(("lights[" + std::to_string(i) + "].color").c_str(), lights[i].Color);
		shader.setVector3f(("lights[" + std::to_string(i) + "].misc").c_str(), lights[i].misc);
	}

	//SHADOW Mapping

	const unsigned int SHADOW_WIDTH = 2048, SHADOW_HEIGHT = 2048;
	unsigned int depthMapFBO;
	glGenFramebuffers(1, &depthMapFBO);
	// create depth texture
	unsigned int depthMap;
	glGenTextures(1, &depthMap);
	glBindTexture(GL_TEXTURE_2D, depthMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	float clampingColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, clampingColor);

	// attach depth texture as FBO's depth buffer
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	unsigned int depthMap2FBO;
	glGenFramebuffers(1, &depthMap2FBO);
	// create depth texture
	unsigned int depthMap2;
	glGenTextures(1, &depthMap2);
	glBindTexture(GL_TEXTURE_2D, depthMap2);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, clampingColor);

	// attach depth texture as FBO's depth buffer
	glBindFramebuffer(GL_FRAMEBUFFER, depthMap2FBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap2, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);


	// shadowMapping for the point lights use cubeMaps
	unsigned int pointDepthMapFBO;
	// create depth texture
	unsigned int pointDepthMap;
	glGenFramebuffers(1, &pointDepthMapFBO);
	glGenTextures(1, &pointDepthMap);
	glBindTexture(GL_TEXTURE_CUBE_MAP, pointDepthMap);
	for (unsigned i = 0; i < 6; i++) 
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);

		
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	// attach depth texture as FBO's depth buffer
	glBindFramebuffer(GL_FRAMEBUFFER, pointDepthMapFBO);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, pointDepthMap, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	shadowMapShader.use();

	dynWorld.addTable(planeTable);
	dynWorld.addPlane(0.0, 0.0, 0.0, 0.0, 0.1, 0.0);
	dynWorld.addCube(8.0, 4.0, 0.25, 0.0, 4.0, 8.0, 0.0);
	dynWorld.addCube(8.0, 4.0, 0.25, 0.0, 4.0, -8.0, 0.0);
	dynWorld.addCube(0.25, 4.0, 8.0, 8.0, 4.0, 0.0, 0.0);
	dynWorld.addCube(0.25, 4.0, 8.0, -8.0, 4.0, 0.0, 0.0);
	dynWorld.addSphere(0.03, sphere1.model[3][0], sphere1.model[3][1] * 2.0, sphere1.model[3][2], 0.2);
	dynWorld.addSphere(0.03, sphere2.model[3][0], sphere2.model[3][1] * 2.0, sphere2.model[3][2], 0.2);
	dynWorld.addSphere(0.03, sphere2.model[3][0] - 0.06, sphere2.model[3][1] * 2.0, sphere2.model[3][2] + 0.06, 0.2);
	dynWorld.addSphere(0.03, sphere2.model[3][0] - 0.06, sphere2.model[3][1] * 2.0, sphere2.model[3][2] - 0.06, 0.2);
	dynWorld.addSphere(0.03, sphere2.model[3][0] - 0.06, sphere2.model[3][1] * 2.0, sphere2.model[3][2], 0.2);
	dynWorld.addSphere(0.03, sphere2.model[3][0] - 0.12, sphere2.model[3][1] * 2.0, sphere2.model[3][2] + 0.06, 0.2);
	dynWorld.addSphere(0.03, sphere2.model[3][0] - 0.12, sphere2.model[3][1] * 2.0, sphere2.model[3][2] - 0.06, 0.2);
	dynWorld.addSphere(0.03, sphere2.model[3][0] - 0.12, sphere2.model[3][1] * 2.0, sphere2.model[3][2], 0.2);
	dynWorld.addSphere(0.03, sphere2.model[3][0] - 0.12, sphere2.model[3][1] * 2.0, sphere2.model[3][2] + 0.12, 0.2);
	dynWorld.addSphere(0.03, sphere2.model[3][0] - 0.12, sphere2.model[3][1] * 2.0, sphere2.model[3][2] - 0.12, 0.2);

	gContactAddedCallback = callbackFunc;
	float timeWOinter = 0;
	//glEnable(GL_FRAMEBUFFER_SRGB);
	glfwSwapInterval(1);
	while (!glfwWindowShouldClose(window)) {

		dynWorld.dynamicsWorld->stepSimulation(1.0 / 60.0);

		btTransform tSphere;
		dynWorld.bodies.at(6)->body->getMotionState()->getWorldTransform(tSphere);
		float mat[16];
		tSphere.getOpenGLMatrix(mat);
		sphere1.model = glm::make_mat4(mat);
		sphere1.model = glm::scale(sphere1.model, glm::vec3(0.03));
		lights.at(1).Position = glm::vec3(sphere1.model[3] + glm::vec4(0.0, 0.03, 0.0, 0.0));


		processInput(window);
		view = camera.GetViewMatrix();
		glfwPollEvents();


		double now = glfwGetTime();
		glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


		glm::mat4 lightProjection, lightView, lightPerspective;
		glm::mat4 lightSpaceMatrix, ligthPerspMatrix;
		float near_plane = 1.0f, far_plane = 100.0f;

		glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
		for (Light l : lights) {

			shadowMapShader.use();
			if (l.misc.x == 2.0) {
				near_plane = 1.0f, far_plane = 100.0f;
				lightProjection = glm::ortho(-35.0f, 35.0f, -35.0f, 35.0f, near_plane, far_plane);
				lightView = glm::lookAt(lightPos, glm::vec3(0.0f), glm::vec3(0.0, 1.0, 0.0));
				lightSpaceMatrix = lightProjection * lightView;

				glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
				glClear(GL_DEPTH_BUFFER_BIT);
				shadowMapShader.setMatrix4("lightSpaceMatrix", lightSpaceMatrix);
			}
			if (l.misc.x == 1.0) {
				near_plane = 0.1f, far_plane = 15.0f;
				lightPerspective = glm::perspective(glm::radians(90.0f), 1.0f, near_plane, far_plane);
				lightView = glm::lookAt(spotLightPos, glm::vec3(0.0f), glm::vec3(0.0, 0.0, 1.0));
				ligthPerspMatrix = lightPerspective * lightView;
				glBindFramebuffer(GL_FRAMEBUFFER, depthMap2FBO);
				glClear(GL_DEPTH_BUFFER_BIT);
				shadowMapShader.setMatrix4("lightSpaceMatrix", ligthPerspMatrix);
			}
			if (l.misc.x == 0.0) {
				near_plane = 1.0f, far_plane = 30.0f;
				lightPerspective = glm::perspective(glm::radians(90.0f), 1.0f, near_plane, far_plane);
				glm::mat4 lightTransforms[] = {
					lightPerspective * glm::lookAt(l.Position, l.Position + glm::vec3(1.0, 0.0, 0.0), glm::vec3(0.0, -1.0, 0.0)),
					lightPerspective * glm::lookAt(l.Position, l.Position + glm::vec3(-1.0, 0.0, 0.0), glm::vec3(0.0, -1.0, 0.0)),
					lightPerspective * glm::lookAt(l.Position, l.Position + glm::vec3(0.0, 1.0, 0.0), glm::vec3(0.0, 0.0, -1.0)),
					lightPerspective * glm::lookAt(l.Position, l.Position + glm::vec3(0.0, -1.0, 0.0), glm::vec3(0.0, 0.0, -1.0)),
					lightPerspective * glm::lookAt(l.Position, l.Position + glm::vec3(0.0, 0.0, 1.0), glm::vec3(0.0, -1.0, 0.0)),
					lightPerspective * glm::lookAt(l.Position, l.Position + glm::vec3(0.0, 0.0, -1.0), glm::vec3(0.0, -1.0, 0.0)),
				};
				glBindFramebuffer(GL_FRAMEBUFFER, pointDepthMapFBO);
				glClear(GL_DEPTH_BUFFER_BIT);
				shadowCubeMap.use();
				shadowCubeMap.setMatrix4("lightSpaceMatrix[0]", lightTransforms[0]);
				shadowCubeMap.setMatrix4("lightSpaceMatrix[1]", lightTransforms[1]);
				shadowCubeMap.setMatrix4("lightSpaceMatrix[2]", lightTransforms[2]);
				shadowCubeMap.setMatrix4("lightSpaceMatrix[3]", lightTransforms[3]);
				shadowCubeMap.setMatrix4("lightSpaceMatrix[4]", lightTransforms[4]);
				shadowCubeMap.setMatrix4("lightSpaceMatrix[5]", lightTransforms[5]);
				shadowCubeMap.setVector3f("lightPos", l.Position);
				shadowCubeMap.setFloat("farPlane", far_plane);
			

				// render scene from light's point of view

				shadowCubeMap.setMatrix4("M", table.model);
				table.draw(shadowCubeMap);
				shadowCubeMap.setMatrix4("M", planeTable.model);
				planeTable.draw(shadowCubeMap);
				shadowCubeMap.setMatrix4("M", sphere2.model);
				sphere2.draw(shadowCubeMap);
				shadowCubeMap.setMatrix4("M", wall1.model);

				for (Object gr : grounds) {
					shadowCubeMap.setMatrix4("M", gr.model);
					gr.draw(shadowCubeMap);
				}
				for (Object gr : walls) {
					shadowCubeMap.setMatrix4("M", gr.model);
					gr.draw(shadowCubeMap);
				}
				// Every sphere rendering
				for (int i = 7; i < dynWorld.bodies.size(); i++) {

					dynWorld.bodies.at(i)->body->getMotionState()->getWorldTransform(tSphere);
					tSphere.getOpenGLMatrix(mat);
					sphere2.model = glm::make_mat4(mat);
					sphere2.model = glm::scale(sphere2.model, glm::vec3(0.03));
					shadowCubeMap.setMatrix4("M", sphere2.model);
					glm::mat4 itM = glm::inverseTranspose(sphere2.model);
					shadowCubeMap.setMatrix4("itM", itM);
					sphere2.draw(shadowCubeMap);
				}

				glBindFramebuffer(GL_FRAMEBUFFER, 0);
			}
			else{ //every non point light
			

				// render scene from light's point of view

				shadowMapShader.setMatrix4("M", table.model);
				table.draw(shadowMapShader);
				shadowMapShader.setMatrix4("M", planeTable.model);
				planeTable.draw(shadowMapShader);
				shadowMapShader.setMatrix4("M", sphere2.model);
				sphere2.draw(shadowMapShader);
				shadowMapShader.setMatrix4("M", wall1.model);

				for (Object gr : grounds) {
					shadowMapShader.setMatrix4("M", gr.model);
					gr.draw(shadowMapShader);
				}
				for (Object gr : walls) {
					shadowMapShader.setMatrix4("M", gr.model);
					gr.draw(shadowMapShader);
				}
				// Every sphere rendering
				for (int i = 7; i < dynWorld.bodies.size(); i++) {

					dynWorld.bodies.at(i)->body->getMotionState()->getWorldTransform(tSphere);
					tSphere.getOpenGLMatrix(mat);
					sphere2.model = glm::make_mat4(mat);
					sphere2.model = glm::scale(sphere2.model, glm::vec3(0.03));
					shadowMapShader.setMatrix4("M", sphere2.model);
					glm::mat4 itM = glm::inverseTranspose(sphere2.model);
					shadowMapShader.setMatrix4("itM", itM);
					sphere2.draw(shadowMapShader);
			}

			glBindFramebuffer(GL_FRAMEBUFFER, 0);
			}
		}


		// reset viewport
		glViewport(0, 0, width, height);


		glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		shadowRender.use();
		shadowRender.setFloat("near_plane", near_plane);
		shadowRender.setFloat("far_plane", far_plane);
		glActiveTexture(GL_TEXTURE10);
		glBindTexture(GL_TEXTURE_2D, depthMap);
		shadowRender.setInteger("depthMap", 10);
		glActiveTexture(GL_TEXTURE11);
		glBindTexture(GL_TEXTURE_2D, depthMap2);
		shadowRender.setInteger("depthMap", 11);

		//renderQuad();


		//Drawing the lights (one cube and one ball)
		lightShader.use();

		lightShader.setMatrix4("V", view);
		lightShader.setMatrix4("P", perspective);
		lightShader.setMatrix4("M", lightCube.model);
		lightShader.setVector3f("color", lights.at(0).Color);
		lightCube.draw();





		//lightShader.setVector3f("color", lights.at(1).Color);
		lightShader.setMatrix4("M", sphere1.model);
		lightShader.setVector3f("color", l2.Color);
		glm::mat4 itM = glm::inverseTranspose(sphere1.model);
		sphere1.draw();

		lightShader.setMatrix4("M", lightSphere.model);
		lightShader.setVector3f("color", spotLight.Color);
		lightSphere.draw();

		if (coll[0] == 2 || coll[1] == 2) {
			l4.Color = glm::vec3(1.0);
			timeWOinter = 1;
		}
		else if (timeWOinter != 0)
			timeWOinter++;
		if (timeWOinter > 120) {
			timeWOinter = 0;
			l4.Color = glm::vec3(0.0);
		}


		lightShader.setMatrix4("M", cube.model);
		lightShader.setMatrix4("itM", glm::inverseTranspose(cube.model));
		lightShader.setVector3f("color", l4.Color);
		cube.draw();

		// Drawing the table
		shader.use();
		lightShader.setVector3f("lights[3].color", l4.Color);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, text1.ID);
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, text2.ID);
		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D, text3.ID);
		glActiveTexture(GL_TEXTURE4);
		glBindTexture(GL_TEXTURE_2D, text4.ID);
		glActiveTexture(GL_TEXTURE5);
		glBindTexture(GL_TEXTURE_2D, text5.ID);
		glActiveTexture(GL_TEXTURE6);
		glBindTexture(GL_TEXTURE_2D, text6.ID);
		glActiveTexture(GL_TEXTURE7);
		glBindTexture(GL_TEXTURE_2D, text7.ID);
		glActiveTexture(GL_TEXTURE8);
		glBindTexture(GL_TEXTURE_2D, text8.ID);
		glActiveTexture(GL_TEXTURE15);
		glBindTexture(GL_TEXTURE_2D, text15.ID);
		glActiveTexture(GL_TEXTURE16);
		glBindTexture(GL_TEXTURE_2D, text16.ID);
		glActiveTexture(GL_TEXTURE17);
		glBindTexture(GL_TEXTURE_2D, text17.ID);
		glActiveTexture(GL_TEXTURE18);
		glBindTexture(GL_TEXTURE_2D, text18.ID);

		glActiveTexture(GL_TEXTURE10);
		glBindTexture(GL_TEXTURE_2D, depthMap);
		glActiveTexture(GL_TEXTURE11);
		glBindTexture(GL_TEXTURE_2D, depthMap2);
		glActiveTexture(GL_TEXTURE12);
		glBindTexture(GL_TEXTURE_CUBE_MAP, pointDepthMap);


		shader.setInteger("shadowMap", 10);
		shader.setInteger("shadowMap2", 11);
		shader.setInteger("shadowCubeMap0", 12);

		shader.setMatrix4("V", view);
		shader.setMatrix4("P", perspective);
		shader.setMatrix4("lightSpaceMatrix", lightSpaceMatrix);
		shader.setMatrix4("lightPerspMatrix", ligthPerspMatrix);
		shader.setVector3f("u_view_pos", camera.Position);

		shader.setVector3f("lightPos2", lights.at(1).Position);


		/*shader.setInteger("u_text", 4);
		shader.setMatrix4("M", cube2.model);
		shader.setMatrix4("itM", glm::inverseTranspose(cube2.model));
		cube2.draw();
		*/


		shader.setInteger("u_text", 5);
		shader.setInteger("normal0", 6);
		auto delta = light_pos + glm::vec3(0.0, 1.0, 2 * std::sin(now));
		//lights[2].Position = delta;
		//What information do you need to gave to the shader about the light ?
		shader.setFloat("light_nb", float(lights.size()));
		for (int i = 0; i < lights.size(); i++) {
			shader.setVector3f(("lights[" + std::to_string(i) + "].pos").c_str(), lights[i].Position);
		}


		// Drawing of the spheres that need the shader1
		for (int i = 7; i < dynWorld.bodies.size(); i++) {

			dynWorld.bodies.at(i)->body->getMotionState()->getWorldTransform(tSphere);
			tSphere.getOpenGLMatrix(mat);
			sphere2.model = glm::make_mat4(mat);
			sphere2.model = glm::scale(sphere2.model, glm::vec3(0.03));
			shader.setMatrix4("M", sphere2.model);
			glm::mat4 itM = glm::inverseTranspose(sphere2.model);
			shader.setMatrix4("itM", itM);
			sphere2.draw();
		}


		shader.setInteger("u_text", 15);
		shader.setInteger("normal0", 16);
		shader.setMatrix4("M", table.model);
		shader.setMatrix4("itM", glm::inverseTranspose(table.model));
		table.draw();


		// Drawing the ground
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, text1.ID);
		//shader.setInteger("u_text", 1);

		shader.setInteger("u_text", 1);
		shader.setInteger("normal0", 2);
		for (Object gr : grounds) {
			shader.setMatrix4("M", gr.model);
			shader.setMatrix4("itM", glm::inverseTranspose(gr.model));
			gr.draw();
		}


		shader.setInteger("u_text", 7);
		shader.setInteger("normal0", 8);
		for (Object gr : walls) {
			shader.setMatrix4("M", gr.model);
			shader.setMatrix4("itM", glm::inverseTranspose(gr.model));
			gr.draw();
		}

		//shader.setInteger("normal0", 0);
		// Drawing the plane of the table

		shader.setInteger("u_text", 3);
		shader.setInteger("normal0", 4);
		//shader.setInteger("u_text", 2);
		shader.setMatrix4("M", planeTable.model);
		shader.setMatrix4("itM", glm::inverseTranspose(planeTable.model));
		planeTable.draw();


		cubeMapShader.use();
		// Drawing the cubeMap
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMapText.ID);
		cubeMapShader.setInteger("cubemapTexture", 0);

		glDepthFunc(GL_LEQUAL);
		cubeMapShader.setMatrix4("V", view);
		cubeMapShader.setMatrix4("P", perspective);
		cubeMap.draw();
		glDepthFunc(GL_LESS);

		// fps counter and swapping the image
		fps(now);
		glfwSwapBuffers(window);
	}

	// Physic world suppression
	for (int i = 0; i < dynWorld.bodies.size(); i++) {
		dynWorld.dynamicsWorld->removeCollisionObject(dynWorld.bodies[i]->body);
		btMotionState* motionState = dynWorld.bodies[i]->body->getMotionState();
		btCollisionShape* shape = dynWorld.bodies[i]->body->getCollisionShape();
		delete dynWorld.bodies[i]->body;
		delete dynWorld.bodies[i];
		delete shape;
		delete motionState;
	}

	delete dynWorld.dynamicsWorld;
	delete dynWorld.solver;
	delete dynWorld.broadphase;
	delete dynWorld.dispatcher;
	delete dynWorld.collisionConfiguration;

	//clean up ressource
	glfwDestroyWindow(window);
	glfwTerminate();

	return 0;
}


int space_pressed = 0;
void processInput(GLFWwindow* window) {

	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		camera.ProcessKeyboardMovement(LEFT, 0.1);
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		camera.ProcessKeyboardMovement(RIGHT, 0.1);

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		camera.ProcessKeyboardMovement(FORWARD, 0.1);
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		camera.ProcessKeyboardMovement(BACKWARD, 0.1);

	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
		camera.ProcessKeyboardRotation(1, 0.0, 1);
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
		camera.ProcessKeyboardRotation(-1, 0.0, 1);

	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
		camera.ProcessKeyboardRotation(0.0, 1.0, 1);
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
		camera.ProcessKeyboardRotation(0.0, -1.0, 1);
	if (glfwGetWindowAttrib(window, GLFW_HOVERED))
	{
		double xpos, ypos;
		//getting cursor position
		glfwGetCursorPos(window, &xpos, &ypos);
		mouse_callback(window, xpos, ypos);
	}

	if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
		if (space_pressed == 0)
		{
			space_pressed = 1;
			glm::vec3 look = camera.Front;
			dynWorld.addSphere(0.03, camera.Position.x, camera.Position.y, camera.Position.z, 0.2, btVector3(look.x * 5.0, look.y * 5.0, look.z * 5.0));

		}
	}
	if (glfwGetKey(window, GLFW_KEY_SPACE) != GLFW_PRESS)
		space_pressed = 0;

}

/* Motivated student can implement the rotation using the mouse
* You can find a strating code in these comments*/
void mouse_callback(GLFWwindow* window, double xposIn, double yposIn)
{
	float xpos = static_cast<float>(xposIn);
	float ypos = static_cast<float>(yposIn);
	if (firstMouse)
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	float xoffset = xpos - lastX;
	float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

	lastX = xpos;
	lastY = ypos;
	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS)
	{
		camera.ProcessMouseMovement(xoffset, yoffset);
	}
}

// Not my debugger, credit to: https://learnopengl.com/Advanced-Lighting/Shadows/Shadow-Mapping
// 
// renderQuad() renders a 1x1 XY quad in NDC
// -----------------------------------------
unsigned int quadVAO = 0;
unsigned int quadVBO;
void renderQuad()
{
	if (quadVAO == 0)
	{
		float quadVertices[] = {
			// positions        // texture Coords
			-1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
			-1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
			 1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
			 1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
		};
		// setup plane VAO
		glGenVertexArrays(1, &quadVAO);
		glGenBuffers(1, &quadVBO);
		glBindVertexArray(quadVAO);
		glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
	}
	glBindVertexArray(quadVAO);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glBindVertexArray(0);

}