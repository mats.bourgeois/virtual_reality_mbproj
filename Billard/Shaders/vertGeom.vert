#version 330 core
in vec3 position;
in vec2 tex_coords;
in vec3 normal;


out DATA
{
    vec3 Normal;
	vec2 texCoord;
    mat4 projection;
    mat4 lightSpaceMatrix;
    mat4 lightPerspMatrix;
	mat4 M;
	vec3 u_view_pos; 
    vec3 lightPos1; 
    vec3 lightPos2; 
    vec3 lightPos3; 
    vec3 lightPos4; 
    vec3 lightPos5;
} data_out;

uniform mat4 M;
uniform mat4 itM;
uniform mat4 V;
uniform mat4 P;
uniform mat4 lightSpaceMatrix;
uniform mat4 lightPerspMatrix;
uniform vec3 u_view_pos; 
uniform vec3 lightPos1;
uniform vec3 lightPos2;
uniform vec3 lightPos3;
uniform vec3 lightPos4;
uniform vec3 lightPos5;



void main()
{
	gl_Position = M* vec4(position, 1.0f);
	data_out.Normal = vec3(itM*vec4(normal,1.0));
	data_out.texCoord = tex_coords;
	data_out.projection = P*V;
	data_out.lightSpaceMatrix = lightSpaceMatrix;
	data_out.lightPerspMatrix = lightPerspMatrix;
	data_out.M = M;
	data_out.u_view_pos = u_view_pos;
	data_out.lightPos1 = lightPos1;
	data_out.lightPos2 = lightPos2;
	data_out.lightPos3 = lightPos3;
	data_out.lightPos4 = lightPos4;
	data_out.lightPos5 = lightPos5;
}