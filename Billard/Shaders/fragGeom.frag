#version 330 core

out vec4 FragColor;
precision mediump float; 

in vec3 v_frag_coord; 
in vec3 v_normal; 
in vec2 v_text;
in vec4 fragPosLight1;
in vec4 fragPosLight2;
in vec3 u_view_pos; 
in vec3 lightPos1; 
in vec3 lightPos2; 
in vec3 lightPos3; 
in vec3 lightPos4; 
in vec3 lightPos5;
in vec3 truePos;

uniform sampler2D u_text;
uniform sampler2D normal0;
uniform sampler2D shadowMap;
uniform sampler2D shadowMap2;
uniform samplerCube shadowCubeMap;

uniform float farPlane;

/*What information do you need for the light ? (position, strength, ...)*/
struct Light{
	vec3 pos;
	vec3 strength;
	vec3 attenuation;
	vec3 color;
	vec3 misc;
};
uniform float light_nb;
uniform Light[50] lights;

/*What information do you need about the object ?*/
uniform vec3 materialColour; 
uniform float shininess;

float specularCalculation(Light l, vec3 N, vec3 L ,vec3 V ){ 
	vec3 R = reflect (-L,N);  /*reflect (-L,N) is  equivalent to max (2 * dot(N,L) * N - L , 0.0) ;*/
	// Blinn Phong Light
	vec3 halfWayVec = normalize(V+L);
	// float cosTheta = dot(R , V); 
	float cosTheta = dot(N, halfWayVec); 
	
	float spec = pow(max(cosTheta,0.0), 32.0); 
	return l.strength.z* spec;
}

float attenuationCalculation(Light l, float distance){
	float res = 1.0 / (l.attenuation.x + l.attenuation.y * distance + l.attenuation.z * distance * distance);
	return res;
}

vec3 pointLight(Light currLight){
	
	/*Compute each of the component needed (specular light, diffuse light, attenuation,...)*/
	vec3 N = normalize(v_normal);
	
	// vec3 N = normalize(texture(normal0, v_text).xyz*2.0f-1.0f);
	vec3 V = normalize(truePos - v_frag_coord);

	vec3 tot = vec3(0.0);
	float att, diff, spec, distance;
	vec3 L;
	L = normalize(lightPos2-v_frag_coord);
	distance = length(lightPos2 - v_frag_coord);
	att= attenuationCalculation(currLight, distance);
	diff=currLight.strength.y * max(dot(N,L),0.0);
	if(diff!=0.0){
		spec = specularCalculation(currLight, N,L, V);
	}
	else{
		spec=0.0f;
	}

	float shadow=0.0f;
	vec3 fragToLight = v_frag_coord-currLight.pos;
	float currentDepth = length(fragToLight);
	// float bias= max(0.5f*dot(N, L), 0.00025f);
	float bias= 0.00025f;
	
	// Smoothens out the shadows
	int sampleRadius = 2;
	float pixelSize = 1.0 /1024.0f;
	float closestDepth;
	closestDepth =  texture(shadowCubeMap, fragToLight).r;
	closestDepth *= farPlane;
	if (currentDepth > closestDepth + bias)
	shadow = 0.0f;   
	/*
	for(int z = -sampleRadius; z <= sampleRadius; z++){
		for(int y = -sampleRadius; y <= sampleRadius; y++){
			for(int x = -sampleRadius; x <= sampleRadius; x++){
				float closestDepth;
				closestDepth =  texture(shadowCubeMap, fragToLight+vec3(x,y,z)*pixelSize).r;
				closestDepth *= farPlane;
				
				if (currentDepth > closestDepth + bias)
					shadow += 1.0f;     
			}
		}
	}
	shadow /= pow((sampleRadius * 2 + 1), 3);
	*/

	tot += att * (diff + spec)*max(dot(N,L),0.0)*currLight.color*(1.0-shadow);

	return tot;

}

vec3 direcLight(Light currLight){

	/*Compute each of the component needed (specular light, diffuse light, attenuation,...)*/
	// vec3 N = normalize(v_normal);
	vec3 N = normalize(texture(normal0, v_text).xyz*2.0f-1.0f);
	vec3 V = normalize(u_view_pos - v_frag_coord);

	vec3 tot = vec3(0.0);
	float att, diff, spec=0.0, distance;
	vec3 L;
	L = normalize(lightPos1-v_frag_coord);
	distance = length(lightPos1 - v_frag_coord);
	att= attenuationCalculation(currLight, distance);
	diff=currLight.strength.y * dot(N,L);
	if(diff!=0){
		spec = specularCalculation(currLight, N,L, V);
	}

	float shadow = 0.0f;
	vec3 lightCoords = fragPosLight1.xyz / fragPosLight1.w;
	if (lightCoords.z <= 1.0f){
		lightCoords= (lightCoords+1.0f)/2.0f;

		// float closestDepth = texture(shadowMap, lightCoords.xy).r;
		float currentDepth = lightCoords.z;
		//float bias= 0.0008f;
		float bias = max(0.0025f * (1.0f - dot(N, L)), 0.0005f);
		// Smoothens out the shadows
		int sampleRadius = 2;
		vec2 pixelSize = 1.0 / textureSize(shadowMap, 0);
		for(int y = -sampleRadius; y <= sampleRadius; y++)
		{
		    for(int x = -sampleRadius; x <= sampleRadius; x++)
		    {
		        float closestDepth =  texture(shadowMap, lightCoords.xy+vec2(x,y)*pixelSize).r;
				if (currentDepth > closestDepth + bias)
					shadow += +1.0f;     
		    }    
		}
		shadow /= pow((sampleRadius * 2 + 1), 2);
	}
	
	tot += att * (diff + spec)*dot(N,L)*currLight.color*(1.0f-shadow) ;

	
	return tot;
}


vec3 spotLight(Light currLight){
	
	/*Cos values of the cones*/
	float extCone = 0.90;
	float inCone = 0.95;

	/*Compute each of the component needed (specular light, diffuse light, attenuation,...)*/
	vec3 N = normalize(v_normal);
	vec3 V = normalize(truePos - v_frag_coord);

	vec3 tot = vec3(0.0);
	float att, diff, spec, distance;
	vec3 L;
	L = normalize(currLight.pos-v_frag_coord);
	distance = length(currLight.pos - v_frag_coord);
	att= attenuationCalculation(currLight, distance);
	diff=currLight.strength.y * max(dot(N,L),0.0);
	spec = specularCalculation(currLight, N,L, V);
		
	/*Calculate angle between object that is lightened and vertical light*/
	float objAngle = dot(vec3(0.0, -1.0, 0.0),-L);
	float inten = clamp((objAngle-extCone)/(inCone-extCone), 0.0f, 1.0f);

	float shadow = 0.0f;
	vec3 lightCoords = fragPosLight2.xyz / fragPosLight2.w;
	if (lightCoords.z <= 1.0f){
		lightCoords= (lightCoords+1.0f)/2.0f;

		float closestDepth = texture(shadowMap2, lightCoords.xy).r;
		float currentDepth = lightCoords.z;
		float bias= 0.00005f;
		if(currentDepth > closestDepth+bias)
			shadow = 1.0f;
	}
	/*Compute the value for the light */
	
	tot += att * (diff + spec)*inten*max(dot(N,L),0.0)*currLight.color*(1.0f-shadow);

	return tot;

}

void main() { 
	vec3 tot = vec3(0.0);
	Light lightI;
	for (float i = 0.0; i < light_nb; i++){
		lightI = lights[int(i)];
		if(i == 0.0)
			lightI.pos = lightPos1;
		if(i == 3.0)
			lightI.pos = lightPos4;
		if(lightI.misc.x==0.0)
			tot += pointLight(lightI);
		else if(lightI.misc.x==1.0)
			tot += spotLight(lightI);
		else
			tot += direcLight(lightI);
	}

	/*Compute the value for the light */
	vec4 textureColors = texture(u_text, v_text);
	vec3 finalColor =min(lights[0].strength.x+tot,1.0)*textureColors.xyz;
	// vec3 finalColor =textureColors.xyz;
	FragColor = vec4(finalColor,1.0);  
} 