#version 330 core
out vec4 FragColor;
precision mediump float;

in vec3 v_frag_coord;
in vec3 v_normal;
in vec2 v_text;

uniform vec3 color;

void main() { 
	FragColor = vec4(color, 1.0); 
}