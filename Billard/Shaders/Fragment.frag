#version 330 core

out vec4 FragColor;
precision mediump float; 

in vec3 v_frag_coord; 
in vec3 v_normal; 
in vec2 v_text;
in vec4 fragPosLight1;
in vec4 fragPosLight2;

uniform vec3 u_view_pos; 
uniform sampler2D u_text;
uniform sampler2D normal0;
uniform sampler2D shadowMap;
uniform sampler2D shadowMap2;

/*What information do you need for the light ? (position, strength, ...)*/
struct Light{
	vec3 pos;
	vec3 strength;
	vec3 attenuation;
	vec3 color;
	vec3 misc;
};
uniform float light_nb;
uniform Light[50] lights;

/*What information do you need about the object ?*/
uniform vec3 materialColour; 
uniform float shininess;

float specularCalculation(Light l, vec3 N, vec3 L ,vec3 V ){ 
	vec3 R = reflect (-L,N);  /*reflect (-L,N) is  equivalent to max (2 * dot(N,L) * N - L , 0.0) ;*/
	// Blinn Phong Light
	vec3 halfWayVec = normalize(V+L);
	// float cosTheta = dot(R , V); 
	float cosTheta = dot(N, halfWayVec); 
	
	float spec = pow(max(cosTheta,0.0), 32.0); 
	return l.strength.z* spec;
}

float attenuationCalculation(Light l, float distance){
	float res = 1.0 / (l.attenuation.x + l.attenuation.y * distance + l.attenuation.z * distance * distance);
	return res;
}

vec3 pointLight(Light currLight){
	
	/*Compute each of the component needed (specular light, diffuse light, attenuation,...)*/
	vec3 N = normalize(v_normal);
	
	// vec3 N = normalize(texture(normal0, v_text).xyz*2.0f-1.0f);
	vec3 V = normalize(u_view_pos - v_frag_coord);

	vec3 tot = vec3(0.0);
	float att, diff, spec, distance;
	vec3 L;
	L = normalize(currLight.pos-v_frag_coord);
	distance = length(currLight.pos - v_frag_coord);
	att= attenuationCalculation(currLight, distance);
	diff=currLight.strength.y * max(dot(N,L),0.0);
	if(diff!=0){
		spec = specularCalculation(currLight, N,L, V);
	}
	else{
		spec=0.0f;
	}
			
	tot += att * (diff + spec)*max(dot(N,L),0.0)*currLight.color;

	return tot;

}

vec3 direcLight(Light currLight){

	/*Compute each of the component needed (specular light, diffuse light, attenuation,...)*/
	// vec3 N = normalize(v_normal);
	vec3 N = normalize(texture(normal0, v_text).xyz*2.0f-1.0f);
	vec3 V = normalize(u_view_pos - v_frag_coord);

	vec3 tot = vec3(0.0);
	float att, diff, spec=0.0, distance;
	vec3 L;
	L = normalize(currLight.pos-v_frag_coord);
	distance = length(currLight.pos - v_frag_coord);
	att= attenuationCalculation(currLight, distance);
	diff=currLight.strength.y * dot(N,L);
	if(diff!=0){
		spec = specularCalculation(currLight, N,L, V);
	}

	float shadow = 0.0f;
	vec3 lightCoords = fragPosLight1.xyz / fragPosLight1.w;
	if (lightCoords.z <= 1.0f){
		lightCoords= (lightCoords+1.0f)/2.0f;

		float closestDepth = texture(shadowMap, lightCoords.xy).r;
		float currentDepth = lightCoords.z;
		float bias= 0.0008f;
		if(currentDepth > closestDepth+bias)
			shadow = 1.0f;
	}
	
	tot += att * (diff + spec)*dot(N,L)*currLight.color*(1.0f-shadow) ;

	
	return tot;
}

vec3 spotLight(Light currLight){
	
	/*Cos values of the cones*/
	float extCone = 0.90;
	float inCone = 0.95;

	/*Compute each of the component needed (specular light, diffuse light, attenuation,...)*/
	vec3 N = normalize(v_normal);
	vec3 V = normalize(u_view_pos - v_frag_coord);

	vec3 tot = vec3(0.0);
	float att, diff, spec, distance;
	vec3 L;
	L = normalize(currLight.pos-v_frag_coord);
	distance = length(currLight.pos - v_frag_coord);
	att= attenuationCalculation(currLight, distance);
	diff=currLight.strength.y * max(dot(N,L),0.0);
	spec = specularCalculation(currLight, N,L, V);
		
	/*Calculate angle between object that is lightened and vertical light*/
	float objAngle = dot(vec3(0.0, -1.0, 0.0),-L);
	float inten = clamp((objAngle-extCone)/(inCone-extCone), 0.0f, 1.0f);

	float shadow = 0.0f;
	vec3 lightCoords = fragPosLight2.xyz / fragPosLight2.w;
	if (lightCoords.z <= 1.0f){
		lightCoords= (lightCoords+1.0f)/2.0f;

		float closestDepth = texture(shadowMap2, lightCoords.xy).r;
		float currentDepth = lightCoords.z;
		float bias= 0.00005f;
		if(currentDepth > closestDepth+bias)
			shadow = 1.0f;
	}
	/*Compute the value for the light */
	
	tot += att * (diff + spec)*inten*max(dot(N,L),0.0)*currLight.color*(1.0f-shadow);

	return tot;

}

void main() { 
	vec3 tot = vec3(0.0);
	Light lightI;
	for (float i = 0.0; i < light_nb; i++){
		lightI = lights[int(i)];
		if(lightI.misc.x==0.0)
			tot += pointLight(lightI);
		else if(lightI.misc.x==1.0)
			tot += spotLight(lightI);
		else
			tot += direcLight(lightI);
	}

	/*Compute the value for the light */
	vec4 textureColors = texture(u_text, v_text);
	vec3 finalColor =min(lights[0].strength.x+tot,1.0)*textureColors.xyz;
	// vec3 finalColor =textureColors.xyz;
	FragColor = vec4(finalColor,1.0);  
} 