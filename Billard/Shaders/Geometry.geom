#version 330 core

layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

out vec3 v_frag_coord;
out vec3 v_normal;
out vec2 v_text;
out vec4 fragPosLight1;
out vec4 fragPosLight2;
out mat3 TBN;
out vec3 u_view_pos; 
out vec3 lightPos1; 
out vec3 lightPos2; 
out vec3 lightPos3; 
out vec3 lightPos4; 
out vec3 lightPos5;
out vec3 truePos;


in DATA
{
    vec3 Normal;
	vec2 texCoord;
    mat4 projection;
    mat4 lightSpaceMatrix;
    mat4 lightPerspMatrix;
    mat4 M;
    vec3 u_view_pos;
    vec3 lightPos1; 
    vec3 lightPos2; 
    vec3 lightPos3; 
    vec3 lightPos4; 
    vec3 lightPos5;
} data_in[];


// Default main function
void main()
{
    // from TBN equation
    vec3 edge0 = gl_in[1].gl_Position.xyz - gl_in[0].gl_Position.xyz;
    vec3 edge1 = gl_in[2].gl_Position.xyz - gl_in[0].gl_Position.xyz;
    vec2 deltaUV0 = data_in[1].texCoord - data_in[0].texCoord;
    vec2 deltaUV1 = data_in[2].texCoord - data_in[0].texCoord;

    float invFac = 1.0f / (deltaUV0.x * deltaUV1.y - deltaUV1.x * deltaUV0.y);

    vec3 tan = vec3(invFac * (deltaUV1.y * edge0 - deltaUV0.y * edge1));
    vec3 bitan = vec3(invFac * (-deltaUV1.x * edge0 + deltaUV0.x * edge1));
    
    vec3 T = normalize(vec3(data_in[0].M*vec4(tan, 0.0f)));
    vec3 B = normalize(vec3(data_in[0].M*vec4(bitan, 0.0f)));
    vec3 N = normalize(vec3(data_in[0].M*vec4(cross(edge1,edge0), 0.0f)));

    mat3 TBN = mat3(T,B,N);
    // We want to get normal map space in the same space as the variables of the lights
    // Need to inverse TBN but it's orthogonal so inverse = transpose
    TBN = transpose(TBN);

    gl_Position = data_in[0].projection * gl_in[0].gl_Position;
    v_normal = data_in[0].Normal;
    v_text = data_in[0].texCoord;
    v_frag_coord = gl_in[0].gl_Position.xyz;
    fragPosLight1 = data_in[0].lightSpaceMatrix * gl_in[0].gl_Position;
    fragPosLight2 = data_in[0].lightPerspMatrix * gl_in[0].gl_Position;
	u_view_pos = TBN * data_in[0].u_view_pos;
    truePos = data_in[0].u_view_pos;
    lightPos1 = TBN * data_in[0].lightPos1;
    lightPos2 = TBN * data_in[0].lightPos2;
    lightPos3 = TBN * data_in[0].lightPos3;
    lightPos4 = TBN * data_in[0].lightPos4;
    lightPos5 = TBN * data_in[0].lightPos5;

    EmitVertex();

    gl_Position = data_in[1].projection * gl_in[1].gl_Position;
    v_normal = data_in[1].Normal;
    v_text = data_in[1].texCoord;
    v_frag_coord = gl_in[1].gl_Position.xyz;
    fragPosLight1 = data_in[1].lightSpaceMatrix * gl_in[1].gl_Position;
    fragPosLight2 = data_in[1].lightPerspMatrix * gl_in[1].gl_Position;
	u_view_pos = TBN*data_in[1].u_view_pos; 
	truePos = data_in[1].u_view_pos; 
    lightPos1 = TBN*data_in[1].lightPos1;
    lightPos2 = TBN*data_in[1].lightPos2;
    lightPos3 = TBN*data_in[1].lightPos3;
    lightPos4 = TBN*data_in[1].lightPos4;
    lightPos5 = TBN*data_in[1].lightPos5;

    EmitVertex();

    gl_Position = data_in[2].projection * gl_in[2].gl_Position;
    v_normal = data_in[2].Normal;
    v_text = data_in[2].texCoord;
    v_frag_coord = gl_in[2].gl_Position.xyz;
    fragPosLight1 = data_in[2].lightSpaceMatrix * gl_in[2].gl_Position;
    fragPosLight2 = data_in[2].lightPerspMatrix * gl_in[2].gl_Position;
	u_view_pos = TBN*data_in[2].u_view_pos; 
	truePos = data_in[2].u_view_pos; 
    lightPos1 = TBN*data_in[2].lightPos1;
    lightPos2 = TBN*data_in[2].lightPos2;
    lightPos3 = TBN*data_in[2].lightPos3;
    lightPos4 = TBN*data_in[2].lightPos4;
    lightPos5 = TBN*data_in[2].lightPos5;

    EmitVertex();

    EndPrimitive();
}